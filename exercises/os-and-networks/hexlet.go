package main

import (
 "fmt"
 "net/http"
)

func main() {

 // create a new `ServeMux`
 mux := http.NewServeMux()

 // handle `/` route
 mux.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
  fmt.Fprint(res, "Hello World!")
 })

 // handle `/hello/hexlet` route
 mux.HandleFunc("/hello/hexlet", func(res http.ResponseWriter, req *http.Request) {
  fmt.Fprint(res, "Hello Hexlet!")
 })

 // listen and serve using `ServeMux`
 http.ListenAndServe(":9000", mux)

}

